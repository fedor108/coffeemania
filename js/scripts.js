
// переход на мобильный

var MOBILE_WIDTH = 800;

// дешевый и сердитый определятор девайсов и броузеров

if(navigator.userAgent.search("iPad") > -1 || navigator.userAgent.search("Android") > 1 || navigator.userAgent.search("iPhone") > 1) {
	$("html").addClass("touch-device");
} else {
	$("html").addClass("desktop-device");
}

if(navigator.userAgent.search("MSIE") > -1 || navigator.userAgent.search("Trident") > -1) {
	$("html").addClass("msie");
}

if(navigator.userAgent.search("Firefox") > -1) {
	$("html").addClass("firefox");
}

if(navigator.userAgent.search("iPhone") > -1 || navigator.userAgent.search("Trident") > -1) {
	$("html").addClass("iphone");
}

if(navigator.userAgent.search("iPad") > -1) {
	$("html").addClass("ipad");
}

if(navigator.userAgent.search("Android") > -1) {
	$("html").addClass("android");
}

// клонируем элементы куда-нибудь для той или иной версии девайса

function cloneElements(options) {
	if(!options.src || !options.target) {
		console.log("cloneElements не хватает аргументов");
		return;
	} else {
		var target = $(options.target);
		for(var i = 0; i < options.src.length; i++) {
			$(options.src[i]).clone().appendTo(target);
		}
	}
}


// открытие-закрытие меню

$(document).on("click", "a[data-slide-toggle]", function(e) {
	if($(this).attr("data-slide-toggle") == "#menu-bar") {
		toggleMenuBar();
	}
});

function toggleMenuBar() {
	$("#menu-bar").toggleClass("opened");
	$("a[data-slide-toggle=#menu-bar]").toggleClass("opened");
}

function closeMenuBar() {
	$("#menu-bar").removeClass("opened");
	$("a[data-slide-toggle=#menu-bar]").removeClass("opened");
}

function openMenuBar() {
	$("#menu-bar").addClass("opened");
	$("a[data-slide-toggle=#menu-bar]").addClass("opened");
}

// переключалка элементов простая c оповещением менеджера открывалок

$(document).on("click", "a[data-toggle]", function(e) {
	var id = $(this).attr("data-toggle");
	var target = $(id).eq(0);
	var href = $(this).attr("data-href");

	// если мы на мобильной версии, грузим href (если есть)
	if($(window).width() < MOBILE_WIDTH && href) {
		location.assign(href);
	}
	// ежели нет, шлем событие в менеджер открывалок
	else {
		if(target.hasClass("show")) {
			$(document).trigger("pleaseclose"+id, { source: $(this) });
		} else {
			$(document).trigger("pleaseopen"+id, { source: $(this) });
		}
	}
	e.preventDefault();
});

// ещё более простая переключалка

$(document).on("click", "a[data-toggle-simple]", function(e) {
	if($("body").width() <= MOBILE_WIDTH && $(this).attr("data-toggle-mobile") == "false") {
		return;
	}
	var selector = $(this).attr("data-toggle-simple");
	var target = $(selector).eq(0);
	if(target.hasClass("show")) {
		target.removeClass("show");
		target.trigger("data-toggle-close");
		target.fadeOut();
		if(target.hasClass("popup")) {
			$("body").removeClass("no-scroll");
		}
	} else {
		target.addClass("show");
		target.trigger("data-toggle-open");
		target.fadeIn();
		if(target.hasClass("popup")) {
			$("body").addClass("no-scroll");
		}
	}
})


// переключалка в группе (радио)
$(document).on("click", "*[data-radio-switch]", function() {
	var target = $($(this).attr("data-radio-switch"));
	var group = $("*[data-radio-switch-group=" + target.attr("data-radio-switch-group") + "]");
	group.hide()
	target.fadeIn().removeClass("selected");
	target.addClass("selected");
});


// попап туров
$(document).on("data-toggle-open", "*[id^=vtour]", function() {
	var src =  $(this).attr("data-src");

	var iframe = document.createElement("iframe");
	$(this).find("article").append(iframe);
	iframe.setAttribute("src", src);
});

$(document).on("data-toggle-close", "*[id^=vtour]", function() {
	$(this).find("iframe").remove();
});


// менеджер открывалок блока команды

$(document).on("pleaseopen#team", function() {
	$("#team").addClass("show");
	$("#team-3d-wrapper").addClass("show");
	$("#team").data("iscroll-api").refresh();
	$("a[data-toggle=#team]").addClass("active");
	$(document).trigger("pleaseclose#feedback");
	closeMenuBar();
	// если на доставке, затемняем всё кроме
	// бокового меню
	if($("#delivery-iframe").length > 0) {
		if($(".black-overlay").length < 1) {
			$("body").append( $("<div class='black-overlay'></div>").css({
				"position": "fixed",
				"top": "0px",
				"left": "235px",
				"right": "0px",
				"bottom": "0px",
				"background-color": "black",
				"z-index": 9,
				"display": "none"
			}) );
		}
		$(".black-overlay").fadeIn();
	}
});

$(document).on("pleaseclose#team", function() {
	$("#team").removeClass("show");
	$("#team-3d-wrapper").removeClass("show");
	$("a[data-toggle=#team]").removeClass("active");

	$(".black-overlay").fadeOut();
});

$(document).on("click", function(event) {
	if($(event.target).parents("#team").length < 1 && $("#team").hasClass("show") && $(event.target).attr("data-toggle") != "#team") {
		$(document).trigger("pleaseclose#team");
	}
})


// менеджер открывалок отзывов

$(document).on("pleaseopen#feedback", function() {
	$("#feedback").addClass("show");
	$("#feedback").data("iscroll-api").refresh();
	$("#feedback-3d-wrapper").addClass("show");
	$(document).trigger("pleaseclose#team");
	closeMenuBar();
});

$(document).on("pleaseclose#feedback", function() {
	$("#feedback").removeClass("show");
	$("#feedback-3d-wrapper").removeClass("show");
});

// менеджер открывалок попапов

$(document).on("pleaseopen#map", function(event, data) {
	// достатть данные и обновить карту
	$("#map").addClass("show");
	$("#map").fadeIn(function() {
		$("body").addClass("no-scroll");
	});

	// нужно обновить карту из данных
	setTimeout(function() {
		var mapElement = $("#map").find("div[data-google-map]");
		mapElement.attr("data-lat", data.source.attr("data-lat"));
		mapElement.attr("data-lng", data.source.attr("data-lng"));
		mapElement.attr("data-zoom", data.source.attr("data-zoom"));
		mapElement.attr("data-marker", data.source.attr("data-marker"));
		initializeGoogleMaps(mapElement);
	}, 100);

	// название перезадать
	$("#map").find(".caption").text(data.source.attr("data-title"));

});

$(document).on("pleaseopen#food", function(event, data) {
	$("#food").addClass("show");
	$("body").addClass("no-scroll");
	$("#food").fadeIn();
	setTimeout(function() {
		fotorama.data("fotorama").resize({
			width: "100%"
		});
	},100);

	// находим слайд с айди еды
	var id = data.source.attr("data-food-id");
	var fotorama = $("#food").find(".fotorama").eq(0);

	fotorama.data("fotorama").show("foodslide"+id);

	// название перезадать
	$("#map").find(".caption").text(data.source.html());
});


// Открыть/закрыть нотификейшн
function openNotification(jqueryElement) {
	clearTimeout(jqueryElement.data("timeout"))
	jqueryElement.addClass("active");
	jqueryElement.data("timeout", setTimeout(function() {
		jqueryElement.trigger("notify-end");
	}, 4000));
}

$(document).on("notify-end", "#feedback .notify.success", function() {
	var self = $(this);
	if(self.parents("#feedback").hasClass("show")) {
		self.parents("#feedback").eq(0).find("a.close").click();
	}
	setTimeout(function() {
		self.removeClass("active");
	}, 1000);
});

$(document).on("notify-end", "#content .notify.success", function() {
	var self = $(this);
	self.removeClass("active");
});




// создание скроллбаров

function createIScroll(selector) {
	// пустые не давать
	if($(selector).length < 1) return;

	// инициализация
	var result = new IScroll(selector, {
		scrollbars: true,
		mouseWheel: true,
		interactiveScrollbars: true,
		shrinkScrollbars: 'scale',
		fadeScrollbars: true,
		click: true
	});

	// запоминаем апи
	$(selector).data("iscroll-api", result);

	return result;
}


// Google карты

function initializeGoogleMaps(collection) {
	if(!collection) collection = $("div[data-google-map]");
	collection.each(function() {
		var mapElement = $(this);

		// достаём опции
		var mapElementOptions = {
			zoom: mapElement.attr("data-zoom") ? parseInt(mapElement.attr("data-zoom")) : 12,
			lat: mapElement.attr("data-lat") ? parseFloat(mapElement.attr("data-lat")) : 55.752023,
			lng: mapElement.attr("data-lng") ? parseFloat(mapElement.attr("data-lng")) : 37.617499,
			sensor: mapElement.attr("data-sensor") ? mapElement.attr("data-sensor") : false
		};

		// делаем карту
	    var myLatlng = new google.maps.LatLng(mapElementOptions.lat, mapElementOptions.lng);
	    var myOptions = {
			zoom: mapElementOptions.zoom,
			center: myLatlng,
			zoomControl: false,
			mapTypeControl: false,
			scaleControl: false,
			streetViewControl: false,
			overviewMapControl: false,
			panControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	    }
	    var map = new google.maps.Map(mapElement.get(0), myOptions);
	    mapElement.data("map-api", map);

	    // маркер, если надо
	    if(mapElement.attr("data-marker")) {
			var marker = new google.maps.Marker({
				position: myLatlng,
				map: map,
				title: mapElement.attr("data-marker")
			});
			mapElement.data("map-marker", marker);
	    }
	});
}


// Превращаем меню в селекты для мобильных

function convertPlainSubmenu(parent, menuCollection) {
	menuCollection.each(function() {
		var menu = $(this);
		var menuElements = menu.find("li");
		// нужно создать select и в опциях иметь ссылки куда переходить по выбору
		var select = $("<select class='plain-menu' data-hide='desktop'></select>").appendTo(parent);
		var group;
		for(var i = 0; i < menuElements.length; i++) {
			if( (menuElements.eq(i).hasClass("group-divider") || menuElements.eq(i).hasClass("divider")) && menuElements.eq(i).attr("data-hide") != "mobile" ) {
				group = $("<optgroup label='" + menuElements.eq(i).text() + "'></optgroup>").appendTo(select);
			} else if(menuElements.eq(i).attr("data-hide") != "mobile") {
				var option = $("<option>" + menuElements.eq(i).text() + "</option>");
				option.attr("value", menuElements.eq(i).find("a").attr("href"));
				if(group) {
					option.appendTo(group);
				} else {
					option.appendTo(select);
				}
			}
		}
		// какой был выбран пункт?
		var activeMenuIndex = menu.find("a.active").parents("li").index();
		console.log(activeMenuIndex, select);
		// выбрать его в селекте
		select.get(0).selectedIndex = activeMenuIndex;
		select.prop("selectedIndex", activeMenuIndex);
	});
	// по выбору селекта, переходим на страницу
	$(document).on("change", "#submenu select.plain-menu", function(event) {
		var href = $(this).find("option").eq(this.selectedIndex).val();
		// если ссылка на странице, крутим

		if(href.search("#") == 0) {
			$("html, body").animate({
				scrollTop: $(href).position().top - 35
			}, 'ease-out');
		} else {
			location.assign(href);
		}
	});
}


// отражение в меню пункта, до куда домотали контент

function menuReflect(menuElements) {
	// в коллекции - пункты меню, которым через href соответсвуют
	// блоки в контенте.
	// когда мы доматываем до опр. пункта, который есть в меню
	// активируем соотв. пункт меню

	var oldScroll = 0
	$(window).on("scroll", function(event) {
		// направление скроллинга надо понять
		var scrollTop = $(window).scrollTop();
		// проверяем слайды для каждого элемента меню
		menuElements.each(function(index) {
			var menuElement = $(this);
			if(menuElement.find("a").length < 1) {
				return;
			}
			var targetElement = $(menuElement.find("a").attr("href"));
			if(targetElement.length < 1) {
				console.log("not found", menuElement.find("a").attr("href"));
				return;
			}

			var prevTarget = $(menuElements.eq(index == 0 ? 0 : index - 1).find("a").attr("href"));
			var nextTarget = $(menuElements.eq(index == menuElements.length - 1 ? menuElements.length - 1 : index + 1).find("a").attr("href"));
			var targetTop = targetElement.position().top;
			var targetBottom = targetElement.position().top + targetElement.outerHeight();

			// если предыдущий элемент скрылся из виду
			if(prevTarget.get(0).getBoundingClientRect().top < 0 && targetElement.get(0).getBoundingClientRect().top > 0) {
				menuElement.find("a").addClass("active");
			} else {
				menuElement.find("a").removeClass("active");
			}

			// А также если первый элемент виден
			if(prevTarget.get(0) == targetElement.get(0) && targetElement.get(0).getBoundingClientRect().top > 0) {
				menuElement.find("a").addClass("active");
			}
		});
		// запоминаем новый скролл
		oldScroll = scrollTop;
	});
	// бам (сразу)
	$(window).trigger("scroll");
}


// плавная прокрутка

function anchorScroll(selector) {
	var stop = false;
	$(document).on("click", selector, function(event) {
		var slide = $($(this).attr("href"));
		padding = parseInt($(this).attr("data-anchor-scroll"));
		// если слайд первый, то не надо padding
		$("html, body").animate({
			scrollTop: slide.position().top - padding
		}, 'ease-out');
		event.preventDefault();
	});
}


// дропдауны

function initDropdown(collection, callback) {
	collection.each(function() {
		// делаем элемент выбранного пункта
		var dropdown = $(this);
		var selected = $("<a class='selected'>" + dropdown.find("a.active").html() + "</a>").prependTo(dropdown);

		dropdown.on("click", "li a", function(event) {
			selected.html($(this).html());
			dropdown.find("a.active").removeClass("active");
			$(this).addClass("active");
			dropdown.find("ul").slideUp(function() {
				dropdown.parents(".iscroll-wrapper").data("iscroll-api").refresh();
			});
			// также скроллер рефрешим

			// колбэк
			if(callback) {
				callback(event, $(this));
			}
			//event.preventDefault();
		});

		selected.on("click", function() {
			dropdown.find("ul").slideToggle(function() {
				// также скроллер рефрешим
				dropdown.parents(".iscroll-wrapper").data("iscroll-api").refresh();
			});
		});
	});
}

// боковое меню для меню еды - прячем лишние

function showRestaurantMenu(index) {
	var ul = $("#submenu").find(".plain-menu").hide().eq(index).show();
}


// после сборки страницы

$(document).on("ready", function() {

	// скроллбары
	createIScroll("#menu-bar.iscroll-wrapper");
	createIScroll("#team.iscroll-wrapper");
	createIScroll("#feedback.iscroll-wrapper");
	createIScroll("#submenu.iscroll-wrapper");

	// чтоб можно было выделять текст
	$("#feedback.iscroll-wrapper").find("form").on("mousedown", function() {
		$("#feedback.iscroll-wrapper").data("iscroll-api").disable();
	});
	$("#feedback.iscroll-wrapper").find("form").on("mouseup", function() {
		$("#feedback.iscroll-wrapper").data("iscroll-api").enable();
	});

	// вычисления начальные для мобильной версии
	if($(window).width() < MOBILE_WIDTH) {
		// минимальную высоту ставим контенту
		$("#content").css({
			"min-height": ($(window).height() - 60 - $("body > footer").outerHeight()) + "px"
		});
	}
	// для десктопной
	else {
		// минимальную высоту ставим контенту
		$("#content").css({
			"min-height": "100%"
		});
	}

	// карты
	if($("div[data-google-map]").length > 0) {
		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyC6BSyQWR6U7xebF3fwnixcgXv1DpKeanw&sensor=false&callback=initializeGoogleMaps";
		document.body.appendChild(script);
	}


	// инициализируем меню для мобильных
	// (пока без сложностей с третьим уровнем)
	convertPlainSubmenu($("#submenu").find(".iscroll-content"), $("#submenu").find("nav[data-dropdown] ul"));
	convertPlainSubmenu($("#submenu").find(".iscroll-content"), $("#submenu").find("ul.plain-menu"));


	// инициализируем рефлексивное меню (отражает докуда домотали)
	$("ul[data-track-scroll]").each(function() {
		// нужно достать все элементы, кроме дивайдера
		menuReflect($(this).find("li").not(".group-divider").not(".divider"));
	});

	// затем плавный скролл
	anchorScroll("a[data-anchor-scroll]");

	// при загрузке страницы, мотаем до нужного хэша
	if(location.href.split("#")[1]) {
		var target = $("#" + location.href.split("#")[1]);
		console.log(target);
		if(target.length > 0) {
			console.log("fdfd")
			$("html, body").animate({
				scrollTop: target.position().top - 50
			}, 'ease-out');
		}
	}

	// дропдауны
	// Для начала спрячем все второго уровня меню
	if($("nav[data-dropdown]").length > 0) {
		// $("nav[data-dropdown]").each(function() {
		// 	var activeId = $(this).find("a.active").parents("li").eq(0).index();
		// 	$(this).siblings("ul.plain-menu[id]").hide().eq(activeId).show();
		// });

		initDropdown($("nav[data-dropdown]"), function(event, anchor) {
			// при клике на дропдаун верхнего уровня
			// заменяем видимый список второго уровня меню
			// считаем что все Plain-menu с айди - такие меню второго уровня
			//$("#submenu").find("ul.plain-menu[id]").hide();
			//$("#submenu").find(anchor.attr("href")).fadeIn();

		});
	}


	// фоторама еды
	// сначала тайно показываем попап чтоб инициализировать фотораму,
	// потом опять прячем
	$("#food").css({ "visibility": "hidden", "display": "block" });
	var foodFotoramaElement = $("#food").find(".fotorama").eq(0);
	foodFotoramaElement.fotorama();
	$("#food").css({ "visibility": "visible", "display": "none"	});

	// Спецпопапы с фоторамой
	$(".popup").each(function(){
		if($(this).css("display") == "none" && $(this).find(".fotorama").length > 0) {
			var popup = $(this);
			popup.css({ "visibility": "hidden", "display": "block" });
			var fotoramaElement = popup.find(".fotorama").eq(0);
			fotoramaElement.fotorama();
			popup.css({ "visibility": "visible", "display": "none" });
		}
	})

	// // фоторама фоток ресторанов
	// // сначала тайно показываем попап чтоб инициализировать фотораму,
	// // потом опять прячем
	// $("section[id^='restaurant']").each(function() {
	// 	var restaurantPopup = $(this);
	// 	restaurantPopup.css({ "visibility": "hidden", "display": "block" });
	// 	var foodFotoramaElement = restaurantPopup.find(".fotorama").eq(0);
	// 	console.log(foodFotoramaElement.get(0));
	// 	foodFotoramaElement.fotorama();
	// 	restaurantPopup.css({ "visibility": "visible", "display": "none" });
	// });

	// // фоторама фоток прессы
	// // сначала тайно показываем попап чтоб инициализировать фотораму,
	// // потом опять прячем
	// $("section[id^='press']").each(function() {
	// 	var popup = $(this);
	// 	popup.css({ "visibility": "hidden", "display": "block" });
	// 	var fotoramaElement = popup.find(".fotorama").eq(0);
	// 	console.log(fotoramaElement.get(0));
	// 	fotoramaElement.fotorama();
	// 	popup.css({ "visibility": "visible", "display": "none" });
	// });

	// на странице промо приложений
	var appPromo = $(".app-promo");
	if(appPromo.length > 0) {
		$(document).on("click", ".app-promo li", function(event) {
			console.log("dfdf")
			$(this).parents("ul").find("li.active").removeClass("active");
			$(this).addClass("active");
			//event.preventDefault();
		});
		//appPromo.find("li").eq(0).addClass("active");
	}

});

// вещи после загрузки картинок

$(window).on("load", function() {

	// превращаем картинки svg в инлайновые
	$("img[src$='.svg']").each(function() {
		var img = $(this);
		$.get(img.attr("src"), function(result) {
			var svg = $(result).find("svg");
			svg.attr("class", img.attr("class"));
			img.after(svg);
			img.remove();
		})
	});


	$("#menu-bar.iscroll-wrapper").data("iscroll-api").refresh();
	if($("#team.iscroll-wrapper").length > 0) {
		$("#team.iscroll-wrapper").data("iscroll-api").refresh();
	}
	if($("#submenu.iscroll-wrapper").length > 0) {
		$("#submenu.iscroll-wrapper").data("iscroll-api").refresh();
	}
	if($("#feedback.iscroll-wrapper").length > 0) {
		$("#feedback.iscroll-wrapper").data("iscroll-api").refresh();
	}



});

// вещи, случающиеся при ресайзе

(function() {
	var prevWidth = $(window).width();
	$(window).on("resize", function() {
		var w = $(window).width();
		if(prevWidth < MOBILE_WIDTH && w >= MOBILE_WIDTH) {
			prevWidth = w;
			toDesktop();
		} else if(prevWidth >= MOBILE_WIDTH && w < MOBILE_WIDTH) {
			prevWidth = w;
			toMobile();
		}

		// видяшки подгоняем под размер
		$(".video").find("iframe").each(function() {
			var w = $(this).width();
			var h = w * 300 / 515;
			$(this).css({
				"height": h+"px"
			});
		})
	});
})();

function toDesktop() {
	console.log("to desktop");

	// минимальную высоту ставим контенту
	$("#content").css({
		"min-height": "100%"
	});
}

function toMobile() {
	console.log("to mobile");

	// если открыта форма отзывов, то загружаем соотв. страницу
	if($("#feedback-3d-wrapper").hasClass("show")) {
		location.assign($(".leave-feedback").find("a[data-toggle=#feedback]").eq(0).attr("data-href"));
	}

	// если открыт блок команды, то загружаем соотв. страницу
	if($("#team").hasClass("show")) {
		location.assign($(".team").find("a[data-toggle=#team]").eq(0).attr("data-href"));
	}

	// минимальную высоту ставим контенту
	$("#content").css({
		"min-height": ($(window).height() - 60 - $("body > footer").outerHeight()) + "px"
	});
}

/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// CommonJS
		factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write

		if (value !== undefined && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setTime(+t + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {};

		// To prevent the for loop in the first place assign an empty array
		// in case there are no cookies at all. Also prevents odd result when
		// calling $.cookie().
		var cookies = document.cookie ? document.cookie.split('; ') : [];

		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			var name = decode(parts.shift());
			var cookie = parts.join('=');

			if (key && key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		if ($.cookie(key) === undefined) {
			return false;
		}

		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

}));


/*	Предупреждение про 18 лет */
$(document).on("ready", function () {
	if ($.cookie("18p") != "ok") {
		restrictAge();
	} else if (window.localStorage) {
		if (window.localStorage.getItem("18p") != "ok") {
			restrictAge();
		}
	}
});

function restrictAge(toggle) {
	var ageRestrictionElement = $('<article id="age-restriction" style="display: none;">'+
		''+
		'<img class="icon-18plus" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAALi0lEQVR4AdVaC5AURxnunrkHz4MjcLt7obCIipqEhxZYiVGDpVix7o4ENJdSAxKpQBQsTBFNwvNIfOVRMRYU0cRAKlJYIWiEW5QyoUgqImWMAUVCpSgqEM3u7B14IEjg2O32+4ebpbtnZm93brPcdd3edP+v/v+///77McNZGcqOROKzQvJmzuX1jPEYY7IeT14G0YoIKSGzC7IzXMo/yxxPNnemX1UIIlX7pOT2WOw6m9s/ZZxdF6n3PjPJPTkp757pOH+NKiqyA5KJxAIu+ToYXx2183LwISy6pWCLWjKpX0aRZ0VhSsYb7+WM/+JyG0+6YwRrLIs9RTpFsaXkCNgRa7yPWezHZmeSyVe54FullG8xyc5KxjE45SucSficDeGcf0Ra8lY0Pu2TLtj9TZnUT3zwAoCSHNAeS9xvWfxHmjzJLjAmFjQ5zjMa/H1u7IjH5zFmPWlGoRByWUsm7RugMHWKdkAylljGLf5DTZCUWYzLbc3p9G81eIUayEOzEWfPYcGpUruUQi5vzqT1gVIJlHpRDkBHyxFyP1D4sBrJrGDyqy2Os1WDV7jRHo9/xWL81z4nMLkCA6MPWIBuvSbBZDy+0jQe8zwnpLj9chtP9pAOpAvppNpHOmN/skKFBdULOgDGr+LcekBlREdCSj63JZN5ToVfzjrpQjqRbroe/EEaQB2mt0IdgCSzGsavUcmpA+zv5mHN3azC+0OddCLdTCfQAJItYToG5oD2eGObxZnG5BrP5PxKZ/swxcPgtDpgCX4ay6U2uEKyNS1Oqs3k04gIiQ3FGtN4JDws87ziS52pbDFtd4Cgq6uzwkA2kW0KyK1qDgDBAwijVRoRjMdSd1dzJvW0Bu/HDdKVdDadQLaRjarq+SmAZHEPouYRFUl1TPtvNzvOEyZ8ILRh07dg03pTV9j0Pdj0KMFdB2xraJxhW3JnwLz5DubNOlPAQGojny1G+K9VdaZ8lhP8pps7Ui/yLYzZQ+KJAzD+YyoRtpTfxZbyZyrs/ahT/1Wx2BXofwjJh3Jns5nMiVbGtHW9L31jC78EW/jHVRno59BZJz2RtzckZlk217ayyHiPYxd1t8qg1rfH41+yJJ+BQ9EwhFB+GuH0I7E1PY2Nye+xNu9SedQ67hHG25Z1J1hvAvxq/GpVPOrn8DsEcTu7pXxyluMcNfAlNzEd1mI6LFYZRU7O5kgKG2n99BDI96mzTupDGIH3PJj6bG9omGxZ9j5sPfOGq3iqU4hdELkJszo6jpg47M7mwFE/x07NHXETb7YxGGcBW4gB2WTiSmk/G4sNHcWtI1A75vHB1mewCshpHoCenIlNYca7dJZ1ZSHjXRlwdZWsSrj0yr9kQ8OncFTeUKzxriw4ClG1EY7HdVv0MjeT+R+sM5wop+EcweOqWMn5X9S2WacwN2HFtrltrzYPLUXx4rTn8hZFHE4kuNyrYWE7EiQbpgKx0T+hts06P28dRIifMeG9tTeNGlWHkPuCj07KowjzFUyK2+h3sS6P+unYjG2jRw/3wUsBSHlcJSfbq1QA1XnOKph9W7pS7/xm9OgJtdXV12OyD8ecGoWYeLi3ka2rrp4KWm3jhe7+1X3uvY/POnnypKpHcsSI9XzI0H8ANtaDE69dVTUV7d0erNSnleOCGRYbzeJEfvn48TQo8ytHMtH4DXhzciFum/NxJh4RsdE0nmiaT53qSg4eSsl5pcHjk2HgS25GcoDZC+7pLyACTLDZHmMCJBMHTNilNuHMgGE+GZfoGcMK84GcEBOhCWnzdnMm808VH1QviwOCBJswybAVMYDIpsjMwYVwfnrc/4aULfH4GETUAduy3TyBPJXbEYvd0JTJFEzqoQJD+okMxg4Jl6d6gYFDdcilVhDOCpDhcQxijHJMPkmibgtu3+jhw54VcwAXQkt0pBBn1sQwxRD+PhxWKJ8Mjz/IYUEwj957VswBuEryhSIm6jdfGDlypKeM98QqUA/cHV7be2J58snwcDAWf4WLtESNSVGxHID3dwex7d4Fwz6vKDG2etDgvyXjg57AnH2L4BbnH8WNzl2o5pdAgiMnvHSL47xJdSovNDR8sMq2Z+D0UUfG48XspIsY7f8N6PNegkA+khCfqWHRqJgDqGMoOR8K78VY5bfJ0PwqYB6htO0VpeqCkNxS8jyb7+G31NePqLHsN8BXd2ncTS7qz3W263D047Frz4pNAeq1KZ0+dj7bPR3D8V9Ni0IN0HZnuz9HGzCPrKa2ls4jdV67L8+KOmBbIjG1pqp6Z0nKw1Dw/IF4+2JoGG+5HIApWri01zeOsxn/I0JxfGFKP5amCfGSDMJ2nz//bklR5BeZh5QrBwRPsHw3mI+17D4Q4csRo0j5Cq4PnkVUvO1ipHUVcsUctLU13OWtZZTQFrV2dZ1CEvyEmgTxMho7QP51VTpyxy60XyQYjtT4nIHNRCrQjtXlckCvEYCOW1Tl3DrOAk1OmpKbyr8b7Q3I3hvAM8+l6/mHNmXxRdTsuWw50oPCdX78VjhNcwBwe5qd1EMeDW6iXsMXLeSUfCnXFCgYAXQURo/askbr0rls93LAVeM9xWSOcKDxAD3PsT2yDLArxKT10VjCumACy+UAU67WHlpTM0oDXGwc6zlVBqAYu/n48RQQ75jI+tpa/zQCEaz3nSuCYKa8ijigmvPBZscIV3zx1Uvh/D8mhegOkAUi3KK+joA57dGjnrNk7hWvHfasiAOyQvhyDRTE+aVwwe2Qz3G8KueTRVJaHacTOWIiEmqzELmZ+HZhcm8nQeILFEaIchZ42XeFRkvbRsYG3XHxCtzXHeGQ1cebCBEQ6h4NbbRQp1/RpSIRcE6IzoCEVtsQa1wQpunoWGIhcPr7AoQNjoMdYTxR4L4IcL/GiiKpAE9rZ+eZHYnGwyCZoJLhjuAxLHfjuMw9z4Q4dg6vYQfZ9jjGrVYksCUqrVvn/PBch663y1ZkFZzahXDMX43jc6PQS4q+dIv5vBUhvUyVgX5ttJcybi9lls3UpMBVwp46vk15PgBcPMiWWI4VyZJ10XuBjCoBDrlGbRdTx2hhaupFIJRUCM9m12IadKqwkupSdnCRXVcSj0HMpXHJwpmDHCBf0+n4vLaA20idRm8h+/puaril3wA1dXY6WcFnwQmlz2HwgHc2ydB7Lr7VBpsw5W5XObDN2msJIbarQITltVPjcdqhFV0QNZsN4n1vOM4hA8bwOnoPy2UnY6lah/A4YeLNNtGAdm1W5Ca5vCZBCe1pCXznyLkW3fikahtvg2emxRvxspNNysuDRfimZiFejz+Vh/VSoVvZaiHqcPmb+7vj/LuNsWwhFuCtKbHYNUgCV4PnSrrZcek57gqEeBfXX2/uz2QOgs43vQrJDcLh9fiduJLGt82IVa9I+TrOIZ90AfjYcDoS1Es9SekiSQQneLL70zPE+Cym1HSKKncfgI8NX8YI6N8DwBvkNRLQnwwqRZdA4yEAK9ISb0rlN0L4sGgtksKDWgcD2AmhxsNGfB+03rPz0pzogWBjQl+KrfQI3OcAmw6FjU+tUm3zOYCQA9kJpRhPtgY6YKA6oVTjCzpgoDkhivG9OmCgOCGq8UU5oL87oS/GF+2A/uqEvhpPdtFxtKiy+czp3V8bNhxfurAb8wzYJ6C0AG5NOHP6Ty+XYdual12g0oabrHvijavxtvNRbXsLHtrL4CpcW+oKiApfBcKYApdI6pixw/jK61fYMuznwvKdDsPklQLH6+2RcPgUXJjMwfL1YZO3VOOJP3QZNIWr7TAnqDSVrkcxnnSM5ABixJuYpWB/CCNS9DQivnIXRFwOr7y+35JOPxZFdmQHUGfJMYnPIIushxOujdJ5X3kw7fbncmyxd7CJIq9PDujpkCcbEl/EJ5C3ICKmYDSuwGdzI/Esh+y8TXi5KehlCk5yHTB8H/r4HQ41u/MEESv/B7c1fQpyKpQUAAAAAElFTkSuQmCC"/>'+

		'<h1>Внимание</h1>'+

		'<p class="actions mobile" style="display: none;">'+
			'<a onclick="restrictAgeOk(true);" class="button medium">Мне есть 18 лет</a>'+
			'<a href="http://yandex.ru" class="button medium" style="opacity: 0.5; margin-left: 20px;">Уйти с сайта</a>'+
		'</p>'+


		'<div class="columns">'+
			'<p>Сайт содержит информацию, не рекомендованную для лиц, не достигших совершеннолетнего возраста. Алкоголь противопоказан лицам до 18 лет, беременным и кормящим женщинам, лицам с заболеваниями центральной нервной системы и органов пищеварения.</p>'+
			'<p>Содержащаяся на сайте справочная информация об ассортименте алкогольной продукции не является предложением о приобретении и доводится до сведения посетителей в соответствии с требованиями Закона РФ от 07.02.1992 № 381-ФЗ</p>'+
		'</div>'+

		'<p class="actions">'+
			'<a onclick="restrictAgeOk(true);" class="button medium">Мне есть 18 лет</a>'+
			'<a href="http://yandex.ru" class="button medium" style="opacity: 0.5; margin-left: 20px;">Уйти с сайта</a>'+
		'</p>'+
	'</article>');
	$("body").prepend(ageRestrictionElement);
	$("#age-restriction").fadeIn(400);
	overlayToggle(true);
}

function restrictAgeOk(toggle) {
	if (toggle) {
		$("#age-restriction").fadeOut(400, function () {
			$("#age-restriction").remove();
		});
		overlayToggle(false);
		var d = new Date();
		d.setFullYear(d.getFullYear() + 1);
		$.cookie("18p","ok", { expires: d });
		if (window.localStorage) {
			window.localStorage.setItem("18p", "ok");
		}
	}
}

function overlayToggle(toggle) {
	if (toggle && $(".dark-overlay").length < 1) {
		$("body").append($("<div class='dark-overlay'></div>").css({
			"position": "fixed",
			"top": "0px",
			"left": "0px",
			"right": "0px",
			"bottom": "0px",
			"background-color": "#000000",
			"z-index": "100",
			"opacity": "0.5",
			"display": "none"
		}));
		$(".dark-overlay").fadeIn(700);
		$("body").addClass("overlay");
	} else {
		$(".dark-overlay").fadeOut(500, function () {
			$(".dark-overlay").remove();
		});
		$("body").removeClass("overlay");
	}
}


/*	Опрос через 30 сек */


function storeSave (key, value) {
	if (!window.localStorage) {
		var d = new Date();
		d.setFullYear(d.getFullYear() + 1);
		$.cookie(key, value, { expires: d });
	} else {
		localStorage.setItem(key, value);
	}
}

function storeLoad (key) {
	if (!window.localStorage) {
		return $.cookie(key);
	} else {
		return localStorage.getItem(key);
	}
}


;(function () {
	var count = 0,
		visited = false,
		limit = 25000;

	$(document).on("click", ".site-poll a", function () {
		storeSave("poll-visited", true);
		hidePollLink();
	});

	if(!window.localStorage) {
		// cookie
	} else {
		// count = localStorage.getItem("poll-count") ? parseInt(localStorage.getItem("poll-count")) : count;
		visited = localStorage.getItem("poll-visited") == "true" ? true : false;
	}

	$(document).on("ready", function () {
		// checkTimer();
	});

	// checkTimer();

	function checkTimer () {
		// count = parseInt(storeLoad("poll-count")) || 0;
		visited = storeLoad("poll-visited") == "true" ? true : false;
		console.log(visited, count, $(".site-poll").get(0));

		if (count >= limit) {
			if (!visited) {
				showPollLink();
			}
		} else {
			// storeSave("poll-count", count + 1000);
			count = count + 1000;

			setTimeout(function () {
				checkTimer();
			}, 1000);

		}
	}

	function showPollLink() {
		$(".site-poll").removeAttr("hidden").addClass("show");
	}

	function hidePollLink() {
		$(".site-poll").attr("hidden", "");
	}
}());

//
// Баннер установки приложения
//

var lang = $("html").attr("lang");
new SmartBanner({
	title: "Кофемания",
	author: "Coffeemania",
	daysHidden: 0,
	daysReminder: 0,
	// force: "ios",
	button: (lang == "ru" ? "Смотреть" : 'View'),
	store: {
		ios: "App Store",
		android: "Google Play",
		windows: "Windows Store"
	},
	price: {
		ios: (lang == "ru" ? "Цена: БЕСПЛАТНО в" : 'Price: FREE on the'),
		android: (lang == "ru" ? "Цена: БЕСПЛАТНО в" : 'Price: FREE on the'),
		windows: (lang == "ru" ? "Цена: БЕСПЛАТНО в" : 'Price: FREE on the')
	}
});